<?php

class CourseNodeEventeObject extends CourseObjectNode {
  
  function getNodeTypes() {
    $handlers = course_node_evente_object_course_handlers();
    return array_keys($handlers['object']);
  }

  /**
   * Set the default content type to be created.
   */
  function optionsForm(&$form, &$form_state) {
    parent::optionsForm($form, $form_state);
    if (!$this->getInstanceId()) {
      $desc = $form['node_type']['#options'][$this->getComponent()];
      $form['node_type']['#options'] = array($this->getComponent() => $desc);
    }

    $config = $this->getOptions();

    $form['incomplete_msg'] = array(
      '#type' => 'textfield',
      '#title' => t('Incomplete message'),
      '#default_value' => $config['incomplete_msg'],
    );

    $form['complete_msg'] = array(
      '#type' => 'textfield',
      '#title' => t('Complete message'),
      '#default_value' => $config['complete_msg'],
    );

  }

  /**
   * If course object is saved without configuration, make sure we have a
   * default node type.
   */
  function optionsDefinition() {
    $options = parent::optionsDefinition();
    if (!isset($options['node_type']) && isset($this->config['object_type'])) {
      $options['node_type'] = $this->config['object_type'];
    } 
     

    $options['incomplete_msg'] = 'You have not attended this event yet.';
    $options['complete_msg'] = 'You have attended this event.';

    return $options;
  }


  /**
   * Display status message as course content.
   */
  public function take() {
    return $this->getStatus();
  }

  /**
   * Return a message about the user's status in this object, for when this
   * object is hidden.
   */
  public function getStatus() {
    $grade = $this->getFulfillment()->getGrade();
    $config = $this->getOptions();
    if (!$this->getFulfillment()->getId()) {
      // User has not been given a status yet.
      return $config['incomplete_msg'];
    }

    if ($this->getFulfillment()->isComplete()) {
      // Complete. User given passed status.
      return $config['complete_msg'];
    }
    else {
      // User given a status but it wasn't complete. This means they failed.
      return $config['incomplete_msg'];
    }
  }

  /**
   * @TODO After creating learning content type, and
   *   event - learning schedule, process this two functions.
   *
   *   getReports()
   *   getReport() - get users that are connected, to the learning schedule.
   *
   * @see CourseObject::getReports()
   */
  public function getReports() {
    return array(
      'results' => array(
        'title' => t('Date of learning'),
      ),
    );
  }

  public function getReport($key) {
    module_load_include('inc', 'quiz', 'quiz.admin');
    switch ($key) {
      case 'results':
        return array(
        'title' => t('Quiz results'),
        'content' => drupal_render(drupal_get_form('quiz_results_manage_results_form', $this->node)),
        );
    }
  }

  public function getEvents() {
    $events = array();
    $query = new EntityFieldQuery();
    // Get Date field so we can order events by date. See https://drupal.org/node/2284993
    $date_field = variable_get("course_node_evente_object_date_field_{$this->getNode()->type}");
    $result = $query->entityCondition('entity_type', 'evente')
      // @TODO who id schould be replaced.
      ->propertyCondition('who_id', $this->node->nid)
      ->fieldOrderBy($date_field, 'value')
      ->execute();
    if (!empty($result)) {
      $eids = array_keys($result['evente']);
      $events = evente_load_multiple($eids);
    }

    return $events;
  }

  public function getAttendedEvent() {
    // @TODO add static variable by
    $query = db_select('evente_signup', 'es');

    $query->condition('es.uid', $this->user->uid)
          ->condition('es.attendance', 1);
    $query->join('evente', 'e', 'es.eid = e.evente_id');
    $query->condition('e.who_id', $this->node->nid);
    $query->addField('e', 'evente_id');
    $result = $query->execute()->fetchCol();

    //$events = evente_load_multiple($result);
    //return array_shift($events);
    return $result ? $result[0] : NULL;
  }

  public function getNode() {
    return $this->node;
  }
}