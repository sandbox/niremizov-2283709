<?php

/**
 * @file evente signup object
 */

class EventeSignup extends Entity {
  // Sing up ID.
  //var $sid;
  // Evente ID.
  //var $eid;
  // User id.
  //var $uid;
  // User event attendance.
  //var $attendance;
  //var $bundle = 'evente_signup';
  //var $entityType = 'evente_signup';

  public function __construct($values = array()) {
    parent::__construct($values, 'evente_signup');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'evente_signup/create');
  }

  public function exists($evente_signup) {
    return entity_get_controller('evente_signup')->exists($this);
  }

}

/**
 * The Controller for evente entities
 */
class EventeSignupController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  public function create(array $values = array()) {
    // Add values that are specific to our evente
    $values += array(
      'is_new' => TRUE,
      'sid' => '',
      'eid' => '',
      'uid' => '',
    );
    $evente = parent::create($values);
    return $evente;
  }

  /**
   * We should handle check whatever such eid uid pair is already exists, so
   * we ovveride default save method.
   *
   * @param EventSignup $evente_signup
   *
   * @see EntityAPIController::save()
   */
  public function save($evente_signup, DatabaseTransaction $transaction = NULL) {
    if (empty($evente_signup->sid) && ($sid = $this->exists($evente_signup))) {
      // Check whatever primary key is set, and if it is not
      // check whatever it exists, and if it is than set primary key.
      $evente_signup->sid = $sid;
    }
    return parent::save($evente_signup, $transaction);
  }

  /**
   * Check whatever such singup is already exists, and if it is return id of signup
   * else FALSE.
   *
   * @param EventSignup $evente_signup
   *
   * @return mixed id of signup or FALSE
   */
  public function exists($evente_signup) {
    $exists = db_query('SELECT sid FROM evente_signup WHERE eid=:eid And uid=:uid', array(':eid' => $evente_signup->eid, ':uid' => $evente_signup->uid))->fetchField();
    return $exists ? $exists : FALSE;
  }

  /**
   * Overriding the buildContent function to add entity specific fields
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
    $wrapper = entity_metadata_wrapper('evente_signup', $entity);

    return $content;
  }
}