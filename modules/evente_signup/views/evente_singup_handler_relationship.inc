<?php
/**
 * @file
 * Definition of evente_singup_handler_relationship.
 */

/**
 * Relationship handler adds additional option for node (who_id) field for relationship signup table.
 *
 * @ingroup views_relationship_handlers
 */
class evente_singup_handler_relationship extends views_handler_relationship_groupwise_max {

  function option_definition() {
    $options = parent::option_definition();

    $options['node_relationship'] = array('default' => 'none');
    return $options;
  }

  /**
   * Default form with additional relationship select for node field. (who_id)
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Code below receives list of relationship available for thi handler.
    // It is taken from views_ui_config_item_form() - views/includes/admin.inc
    $view = &$form_state['view'];
    $display_id = $form_state['display_id'];
    $item = $view->get_item($display_id, $type, $id);
    $id = $form_state['id'];

    // A whole bunch of code to figure out what relationships are valid for
    // this item.
    $relationships = $view->display_handler->get_option('relationships');
    $relationship_options = array();

    foreach ($relationships as $relationship) {
      // relationships can't link back to self. But also, due to ordering,
      // relationships can only link to prior relationships.
      if ($id == $relationship['id']) {
        break;
      }
      $relationship_handler = views_get_handler($relationship['table'], $relationship['field'], 'relationship');
      // ignore invalid/broken relationships.
      if (empty($relationship_handler)) {
        continue;
      }

      // If this relationship is valid for this type, add it to the list.
      $data = views_fetch_data($relationship['table']);
      $base = $data[$relationship['field']]['relationship']['base'];
      $base_fields = views_fetch_fields($base, $form_state['type'], $view->display_handler->use_group_by());

      if ($base == 'node') {
        $relationship_handler->init($view, $relationship);
        $relationship_options[$relationship['id']] = $relationship_handler->label();
      }
    }
    $relationship_options = array_merge(array(
      'none' => t('Do not use a relationship')
    ), $relationship_options);

    // Add select list of relationships.
    $form['node_relationship'] = array(
      '#type' => 'select',
      '#title' => t('Relationship with Node'),
      '#description' => t('Use this if you want to join evente filtered by nid value.'),
      '#options' => $relationship_options,
      '#default_value' => $this->options['node_relationship'],
      '#weight' => 0
    );
  }

  /**
   * Run before the view is built.
   *
   * Save node_relationship if option is set
   */
  function set_relationship() {
    parent::set_relationship();
    $relationship = $this->options['node_relationship'];

    // Check to see if the relationship has already processed. If not, then we
    // cannot process it.
    if (empty($this->view->relationship[$relationship]->alias)) {
      return;
    }
    $this->node_relationship = $this->view->relationship[$relationship]->alias;
  }

  /**
   * This is copy of parent::left_query but with one exeption, this
   * method adds adition node argument if $this->node_relationship is set.
   */
  function left_query($options) {

    // Either load another view, or create one on the fly.
    if ($options['subquery_view']) {
      $temp_view = views_get_view($options['subquery_view']);
      // Remove all fields from default display
      unset($temp_view->display['default']->display_options['fields']);
    }
    else {
      // Create a new view object on the fly, which we use to generate a query
      // object and then get the SQL we need for the subquery.
      $temp_view = $this->get_temporary_view();

      // Add the sort from the options to the default display.
      list($sort_table, $sort_field) = explode('.', $options['subquery_sort']);
      $sort_options = array('order' => $options['subquery_order']);
      $temp_view->add_item('default', 'sort', $sort_table, $sort_field, $sort_options);
    }

    // Get the namespace string.
    $temp_view->namespace = (!empty($options['subquery_namespace'])) ? '_'. $options['subquery_namespace'] : '_INNER';
    $this->subquery_namespace = (!empty($options['subquery_namespace'])) ? '_'. $options['subquery_namespace'] : 'INNER';

    // The value we add here does nothing, but doing this adds the right tables
    // and puts in a WHERE clause with a placeholder we can grab later.
    $temp_view->args[] = '**CORRELATED**';

    // Add the base table ID field.
    $views_data = views_fetch_data($this->definition['base']);
    $base_field = $views_data['table']['base']['field'];
    $temp_view->add_item('default', 'field', $this->definition['base'], $this->definition['field']);

    // Add the correct argument for our relationship's base
    // ie the 'how to get back to base' argument.
    // The relationship definition tells us which one to use.
    $temp_view->add_item(
      'default',
      'argument',
      $this->definition['argument table'], // eg 'term_node',
      $this->definition['argument field'] //  eg 'tid'
    );

    if (!empty($this->node_relationship)) {
      // Add node arguemnt if node relationship is set.
      $temp_view->args[] = '**CORRELATED_NODE**';
      $temp_view->add_item(
        'default',
        'argument',
        'node',
        'nid'
      );
    }

    // Build the view. The creates the query object and produces the query
    // string but does not run any queries.
    $temp_view->build();

    // Now take the SelectQuery object the View has built and massage it
    // somewhat so we can get the SQL query from it.
    $subquery = $temp_view->build_info['query'];

    // Workaround until http://drupal.org/node/844910 is fixed:
    // Remove all fields from the SELECT except the base id.
    $fields =& $subquery->getFields();
    foreach (array_keys($fields) as $field_name) {
      // The base id for this subquery is stored in our definition.
      if ($field_name != $this->definition['field']) {
        unset($fields[$field_name]);
      }
    }

    // Make every alias in the subquery safe within the outer query by
    // appending a namespace to it, '_inner' by default.
    $tables =& $subquery->getTables();
    foreach (array_keys($tables) as $table_name) {
      $tables[$table_name]['alias'] .= $this->subquery_namespace;
      // Namespace the join on every table.
      if (isset($tables[$table_name]['condition'])) {
        $tables[$table_name]['condition'] = $this->condition_namespace($tables[$table_name]['condition']);
      }
    }
    // Namespace fields.
    foreach (array_keys($fields) as $field_name) {
      $fields[$field_name]['table'] .= $this->subquery_namespace;
      $fields[$field_name]['alias'] .= $this->subquery_namespace;
    }
    // Namespace conditions.
    $where =& $subquery->conditions();
    $this->alter_subquery_condition($subquery, $where);
    // Not sure why, but our sort order clause doesn't have a table.
    // TODO: the call to add_item() above to add the sort handler is probably
    // wrong -- needs attention from someone who understands it.
    // In the meantime, this works, but with a leap of faith...
    $orders =& $subquery->getOrderBy();
    foreach ($orders as $order_key => $order) {
      // Until http://drupal.org/node/844910 is fixed, $order_key is a field
      // alias from SELECT. De-alias it using the View object.
      $sort_table = $temp_view->query->fields[$order_key]['table'];
      $sort_field = $temp_view->query->fields[$order_key]['field'];
      $orders[$sort_table . $this->subquery_namespace . '.' . $sort_field] = $order;
      unset($orders[$order_key]);
    }

    // The query we get doesn't include the LIMIT, so add it here.
    $subquery->range(0, 1);

    // Clone the query object to force recompilation of the underlying where and
    // having objects on the next step.
    $subquery = clone $subquery;

    // Extract the SQL the temporary view built.
    $subquery_sql = $subquery->__toString();

    // Replace subquery argument placeholders.
    $quoted = $subquery->getArguments();
    $connection = Database::getConnection();
    foreach ($quoted as $key => $val) {
      if (is_array($val)) {
        $quoted[$key] = implode(', ', array_map(array($connection, 'quote'), $val));
      }
      // If the correlated placeholder has been located, replace it with the outer field name.
      elseif ($val === '**CORRELATED**') {
       $quoted[$key] = $this->definition['outer field'];
      }
      elseif ($val === '**CORRELATED_NODE**') {
        $quoted[$key] = $this->node_relationship.'.nid';
      }
      else {
        $quoted[$key] = $connection->quote($val);
      }
    }
    $subquery_sql = strtr($subquery_sql, $quoted);

    return $subquery_sql;
  }
}
