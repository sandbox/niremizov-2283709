<?php

/**
 * @file
 * evente_signup.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function evente_signup_views_default_views() {
  $export = array();

$view = new view();
  $view->name = 'event_signup';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'evente_signup';
  $view->human_name = 'Event Signup';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially*/

  /* Display: Master */
  $handler->display->display_options['title'] = 'Event Signup';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'ещё';
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'evente_signup';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Поле: Глобальный: Счетчик результатов */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Поле: Пользователь: Название */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Поле: Event Sign Up: Attendance */
  $handler->display->display_options['fields']['attendance']['id'] = 'attendance';
  $handler->display->display_options['fields']['attendance']['table'] = 'evente_signup';
  $handler->display->display_options['fields']['comment']['table'] = 'evente_signup';
  $handler->display->display_options['fields']['comment']['field'] = 'comment';
  $handler->display->display_options['fields']['comment']['label'] = 'Комментарий';
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['arguments'] = FALSE;

  $translatables['event_signup'] = array(
    t('Master'),
    t('Event Signup'),
    t('Master'),
    t('Event Signup'),
    t('ещё'),
    t('Применить'),
    t('Сбросить'),
    t('Сортировать'),
    t('По возрастанию'),
    t('По убыванию'),
    t('Элементов на страницу'),
    t('- Все -'),
    t('Пропустить'),
    t('« первая'),
    t('‹ предыдущая'),
    t('следующая ›'),
    t('последняя »'),
    t('Пользователь'),
    t('#'),
    t('Название'),
    t('Посещаемость'),
    t('Комментарий'),
    t('Page')
  );
  $export['event_signup'] = $view;

  return $export;
}
