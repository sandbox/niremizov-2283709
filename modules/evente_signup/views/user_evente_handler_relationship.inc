<?php

/**
 * @file
 * Definition of user_evente_handler_relationship.
 */

/**
 * Relationship handler to return users by events.
 *
 * @ingroup views_relationship_handlers
 */
class user_evente_handler_relationship extends views_handler_relationship  {

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = 'users';

    $evente_signup = $this->query->add_table('evente_signup', $this->relationship);
    $def['left_table'] = $evente_signup;
    $def['left_field'] = 'uid';
    $def['field'] = 'uid';
    $def['type'] = empty($this->options['required']) ? 'LEFT' : 'INNER';

    $join = new views_join();

    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;

    // use a short alias for this:
    $alias = $def['table'] . '_' . $this->table;

    $this->alias = $this->query->add_relationship($alias, $join, 'users', $this->relationship);
  }
}
