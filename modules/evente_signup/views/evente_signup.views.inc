<?php

/**
 * Implements hook_views_data_alter().
 *
 * - base: The new base table this relationship will be adding. This does not
 *   have to be a declared base table, but if there are no tables that
 *   utilize this base table, it won't be very effective.
 * - base field: The field to use in the relationship; if left out this will be
 *   assumed to be the primary field.
 * - relationship table: The actual table this relationship operates against.
 *   This is analogous to using a 'table' override.
 * - relationship field: The actual field this relationship operates against.
 *   This is analogous to using a 'real field' override.
 * - label: The default label to provide for this relationship, which is
 *   shown in parentheses next to any field/sort/filter/argument that uses
 *   the relationship.
 * - skip base: This allows us to not show this relationship if the base is already
 *   'value' so users won't create circular relationships.
 * - 'outer field': The outer field to substitute into the correlated subquery.
 *       This must be the full field name, not the alias.
 *       Eg: 'term_data.tid'.
 * - 'argument table',
 *    'argument field': These options define a views argument that the subquery
 *     must add to itself to filter by the main view.
 *     Example: the main view shows terms, this handler is being used to get to
 *     the nodes base table. Your argument must be 'term_node', 'tid', as this
 *     is the argument that should be added to a node view to filter on terms.
 */

function evente_signup_views_data_alter(&$data) {

  // Desribe relationship between users and evente table.
  // Implicit join is needed for eid_representative relationship to work correctly.
  $data['users']['table']['default_relationship'] = array(
    'evente' => array(
      'table' => 'evente',
      'field' => 'user_evente_uid',
    ),
  );

  $data['users']['table']['join']['evente_signup'] = array(
    // links users table directly to evente_signup via uid
    'left_field' => 'uid',
    'field' => 'uid',
  );

  // The example of eid_representative relationship was taken from taxonomy.views tid_representative.
  $data['users']['eid_representative']['relationship'] = array(
    'title' => t('Representative event'),
    'label' => t('Representative event'),
    'help' => t('Join Evente accordin to two tables: evente_singup and node.'),
    'handler' => 'evente_singup_handler_relationship',
    'relationship field' => 'uid', // Field of table to which we join. Same as field
    'outer field' => 'users.uid', // Field of relationship
    'argument table' => 'users',
    'argument field' => 'uid',
    'base' => 'evente', // Table which we join.
    'field' => 'evente_id', // Field to compare of joined table.
  );

  $data['evente']['user_evente_uid'] = array(
    'title' => t('Users on evente'),
    'help' => t('Relate events to users. This relationship will cause duplicated records if there are multiple users.'),
    'relationship' => array(
      'handler' => 'user_evente_handler_relationship',
      'label' => t('user'),
      'base' => 'users',
    ),
  );

  // Implicit join is needed for eid_representative relationship to work correctly.
  $data['evente_signup']['table']['join'] = array(
    'evente' => array(
      // links evente_signup table directly to evente via eid
      'left_field' => 'evente_id',
      'field' => 'eid',
    ),
  );
}
