<?php

/**
 * @file
 * Example tpl file for theming a single evente-specific theme
 *
 * Available variables:
 * - $status: The variable to theme (while only show if you tick status)
 * 
 * Helper variables:
 * - $evente: The Model object this status is derived from
 */
?>

<div class="evente-status">
  <?php print '<strong>Event Sample Data:</strong> ' . $evente_sample_data = ($evente_sample_data) ? 'Switch On' : 'Switch Off' ?>
</div>