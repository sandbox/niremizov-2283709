<?php

/**
 * @file
 * Sets up the base table for our entity and a table to store information about
 * the entity types.
 */

/**
 * Implements hook_schema().
 */
function evente_schema() {
  $schema = array();

  $schema['evente'] = array(
    'description' => 'The base table for event entities.',
    'fields' => array(
      'evente_id' => array(
        'description' => 'Primary Key: Identifier for a event.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The {evente_type}.type of this evente.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'name' => array(
        'description' => 'The name of the event - a human-readable identifier.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'who_id' => array(
        'description' => 'The person to whom this event. Nid from node table.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'place_id' => array(
        'description' => 'Store place id of taxonomy terms.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Activity status events',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
    ),
  );

  $schema['evente']['fields'] += array(
    'created' => array(
      'description' => 'The Unix timestamp when the model was created.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
    'changed' => array(
      'description' => 'The Unix timestamp when the model was most recently saved.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ),
    'author' => array(
      'description' => 'The user id which created event',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      )
  );
   $schema['evente']['foreign keys'] = array(
    'node_id' => array(
      'table' => 'node',
      'columns' => array('who_id' => 'nid'),
    ),
    'term_id' => array(
      'table' => 'taxonomy_term_data',
      'columns' => array('place_id' => 'tid'),
    ),
  );
  $schema['evente']['primary key'] = array('evente_id');
  $schema['evente']['indexes'] = array(
    'type' => array('type'),
    'who_id' => array('who_id'),
    'place_id' => array('place_id'),
  );

  if (module_exists('project_m')) {
    //This is optional for common case... Need only for erp intergration
    $schema['evente']['fields'] += array(
      'project_id' => array(
        'description' => 'Store id of linked project.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    );
    $schema['evente']['indexes']['project_id'] = array('project_id');
    $schema['evente']['foreign keys']['project_id'] = array(
      'table' => 'node',
      'columns' => array('project_id' => 'nid'),
    );
  }

  $schema['evente_type'] = array(
    'description' => 'Stores information about defined event types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique model type identifier.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this model type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this model type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'node_ref' => array(
        'description' => 'Type of node, which will be reference in who_id field.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'taxonomy_ref' => array(
        'description' => 'Voc id of term that will be referenced in place_id field.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'The weight of this model type in relation to others.',
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data related to this model type.',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}


/**
 * Implementes hook_install().
 *
 * Install new date format types.
 */
function evente_install() {
  evente_install_date_formats();
}

function evente_install_date_formats() {
  $formats = array();
  $date_formats = array();
  $format_types = array();

  $date_formats['year_month_day'] = 'Y-m-d';
  $formats['year_month_day'] = array(
    'type' => 'evente_year_month_day',
    'format' => $date_formats['year_month_day'],
    'locked' => FALSE,
    'is_new' => TRUE
  );
  $format_types['year_month_day'] = array(
    'title' => t('Shift - year monthday'),
    'type' => 'evente_year_month_day',
    'locked' => FALSE,
    'is_new' => TRUE
  );
  $date_formats['hour_min'] = 'G:i';
  $formats['hour_min'] = array(
    'type' => 'evente_hour_min',
    'format' => $date_formats['hour_min'],
    'locked' => FALSE,
    'is_new' => TRUE
  );
  $format_types['hour_min'] = array(
    'title' => t('Shift - hour min'),
    'type' => 'evente_hour_min',
    'locked' => FALSE,
    'is_new' => TRUE
  );

  foreach ($formats as $index => $format) {
    $format_type = $format_types[$index];
    $date_format = $date_formats[$index];
    // Create date format
    system_date_format_save($format);
    // Create date
    system_date_format_type_save($format_type);
    variable_set('date_format_' . $format_type['type'], $date_format);
  }
}

/**
 * Implements hook_m_uninstall().
 */
function evente_uninstall() {
  // Delete date format type
  system_date_format_type_delete('evente_year_month_day');
  system_date_format_type_delete('evente_hour_min');
  // Delete evente_shift date format manually, cause there was no API found for that
  db_delete('date_formats')->condition('type', 'REGEXP(evente_[.]*)')->execute();
}

/**
 * Add author column to {evente} table
 */
function evente_update_7001() {
  $author = array(
      'description' => 'The user id which created event',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
  );
  db_add_field('evente', 'author', $author);
}

/**
 * Add status column to {evente} table
 */
function evente_update_7002() {
  $status = array(
    'description' => 'Activity status events',
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0
  );
  db_add_field('evente', 'status', $status);
}

/**
 * Name of date formate changed, and placed now in evente module.
 */
function evente_update_7003() {
  // Delete deprecated date formatters.
  system_date_format_type_delete('evente_shift_year_month_day');
  system_date_format_type_delete('evente_shift_hour_min');
  // Delete evente_shift date format manually, cause there was no API found for that
  db_delete('date_formats')->condition('type', 'REGEXP(evente_shift_[.]*)')->execute();
  // Install new date formaters.
  evente_install_date_formats();
}