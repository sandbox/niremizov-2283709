<?php

/**
 * @file
 * evente type editing UI.
 */

/**
 * UI controller.
 */
class EventeTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage evente entity types, including adding
      and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the evente type editing form.
 */
function evente_type_form($form, &$form_state, $evente_type, $op = 'edit') {

  if ($op == 'clone') {
    $evente_type->label .= ' (cloned)';
    $evente_type->type = '';
  }
  //Get options for node_ref field
  $nodeTypes = node_type_get_names();
  //Get options for taxonomy_ref field
  $vocabularies = taxonomy_get_vocabularies();
  $vocOptions = array();
  foreach ($vocabularies as $vocabulary) {
    $vocOptions[$vocabulary->machine_name] = $vocabulary->name;
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $evente_type->label,
    '#description' => t('The human-readable name of this evente type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($evente_type->type) ? $evente_type->type : '',
    '#maxlength' => 32,
    //'#disabled' => $evente_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'evente_get_types',
      'source' => array('label'),
    ),
    '#disabled' => isset($evente_type->type) ? TRUE : FALSE,
    '#description' => t('A unique machine-readable name for this evente type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['node_ref'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#options' => $nodeTypes,
    '#default_value' => isset($evente_type->node_ref) ? $evente_type->node_ref : '',
    '#description' => t('Select node type to which this evente type should be connected.'),
    '#disabled' => isset($evente_type->node_ref) ? TRUE : FALSE,
  );
  $form['taxonomy_ref'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#default_value' => isset($evente_type->taxonomy_ref) ? $evente_type->taxonomy_ref : '',
    '#options' => $vocOptions,
    '#required' => TRUE,
    '#description' => t('The vocabulary which supplies the options for this field.'),
    '#disabled' => isset($evente_type->taxonomy_ref) ? TRUE : FALSE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save evente type'),
    '#weight' => 40,
  );
  $form['data'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Title and date field properties.'),
  );
  $form['data']['date_field_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Datefield name'),
    '#default_value' => isset($evente_type->data['date_field_name']) ? $evente_type->data['date_field_name'] : NULL,
    '#required' => TRUE,
    '#disabled' => isset($evente_type->data['date_field_name']) ? TRUE : FALSE,
    '#description' => t('Machine name of bundles date field. It must only contain lowercase letters, numbers, and underscores.'),
    '#machine_name' => array(
      'exists' => 'field_info_field',
    ),
  );
  $form['data']['title_autogen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable title autogeneration.'),
    '#default_value' => isset($evente_type->data['title_autogen']) ? $evente_type->data['title_autogen'] : 0,
    '#description' => t('If you hide title, title will be generated on save.'),
  );
  $form['data']['title_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Title pattern'),
    '#default_value' => isset($evente_type->data['title_pattern']) ? $evente_type->data['title_pattern'] : '',
    '#maxlength' => 255,
    '#description' => t('Pattern of the title.'),
    '#states' => array(
      'visible' => array(
        ':input[name="data[title_autogen]"]' => array('checked' => TRUE),
      ),
      'required' => array(
        ':input[name="data[title_autogen]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['data']['token_help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('evente'),
  );
  $form['data']['node_ref_title'] = array(
    '#title' => t('Node Reference Label'),
    '#type' => 'textfield',
    '#default_value' => isset($evente_type->data['node_ref_title']) ? $evente_type->data['node_ref_title'] : t('Who'),
    '#description' => t('Label of evente type\'s node_ref field.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['data']['term_ref_title'] = array(
    '#title' => t('Term Reference Label'),
    '#type' => 'textfield',
    '#default_value' => isset($evente_type->data['term_ref_title']) ? $evente_type->data['term_ref_title'] : t('Where'),
    '#description' => t('Label of evente type\'s term_ref field.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  //Locking not supported yet
  /*if (!$evente_type->isLocked() && $op != 'add') {
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete evente type'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('evente_type_form_submit_delete')
  );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function evente_type_form_submit(&$form, &$form_state) {
  $evente_type = entity_ui_form_submit_build_entity($form, $form_state);
  $evente_type->save();
  $form_state['redirect'] = 'admin/structure/evente_types';
}

/**
 * Form API submit callback for the delete button.
 */
function evente_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/evente_types/manage/'.$form_state['evente_type']->type.'/delete';
}
