<?php

/**
 * The class used for evente entities
 */
class Evente extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'evente');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'evente/'.$this->evente_id);
  }

  public function getDateFieldName() {
    return entity_get_controller($this->entityType)->getDateFieldName($this);
  }
}

/**
 * The class used for evente type entities
 */
class EventeType extends Entity {

  public $type;
  public $label;

  public function __construct($values = array()) {
    parent::__construct($values, 'evente_type');
  }

  public function getDateFieldName() {
    return entity_get_controller($this->entityType)->getDateFieldName($this);
  }
}

/**
 * The Controller for evente entities
 */
class EventeController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a evente - we first set up the values that are specific
   * to our evente schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the evente.
   *
   * @return
   *   A evente object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our evente
    $values += array(
      'evente_id' => '',
      'is_new' => TRUE,
      'title' => '',
      'created' => '',
      'changed' => '',
      'data' => '',
    );
    $evente = parent::create($values);
    return $evente;
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;
    $type = evente_get_types($entity->type);

    $entity->author = $user->uid;
    if ((isset($type->data['title_autogen']) && $type->data['title_autogen'])) {
      //If type's title autogeneration set, make appropriate actions to the title
      $entity->name = token_replace($type->data['title_pattern'], array('evente' => $entity), array('sanitize' => FALSE, 'clear' => TRUE));
    }

    // Add in created and changed times.
    if ($entity->is_new = isset($entity->is_new) ? $entity->is_new : 0) {
      $entity->created = time();
    }
    $entity->changed = time();
    return parent::save($entity, $transaction);
  }

  /**
   * Overriding the buildContent function to add entity specific fields
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
    $wrapper = entity_metadata_wrapper('evente', $entity);
    $whoPath = entity_uri('node', $wrapper->who_id->value());
    $content['who_id'] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(),
      '#type' => 'link',
      '#title' => $wrapper->who_id->title->value(),
      '#href' => $whoPath['path'],
      '#weight' => -5,
    );
    $placePath = entity_uri('taxonomy_term', $wrapper->place_id->value());
    $content['place_id'] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(),
      '#type' => 'link',
      '#title' => $wrapper->place_id->name->value(),
      '#href' => $placePath['path'],
      '#weight' => -5,
    );
    $authorPath = entity_uri('user', $wrapper->author->value());
    $content['author'] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(),
      '#type' => 'link',
      '#title' => $wrapper->author->name->value(),
      '#href' => $authorPath['path'],
      '#weight' => -5,
    );
    return $content;
  }

  public function getDateFieldName($entity) {
    return evente_get_types($entity->type)->getDateFieldName();
  }
}

/**
 * The Controller for evente entities
 */
class EventeTypeController extends EntityAPIControllerExportable {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a evente type - we first set up the values that are specific
   * to our evente type schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the evente.
   *
   * @return
   *   A evente type object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our evente
    $values += array(
      'id' => '',
      'is_new' => TRUE,
      'data' => '',
    );
    $evente_type = parent::create($values);
    return $evente_type;
  }

  /**
   * Added creation of date field for new entity type.
   *
   * @param $entity
   *  Entity to be saved. If $entity->data['date_field_save_handeled'] is
   * set to true, field won't be created. This parameter means that save of
   * date field will be handled in other place.
   *
   * @see EntityAPIControllerExportable::save()
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (($save_status = parent::save($entity, $transaction)) == SAVED_NEW && empty($entity->data['date_field_save_handeled'])) {
      // After evente bundle was created, create date field for it.
      $field_name = $entity->data['date_field_name'];
      $field = array(
        'field_name' => $field_name,
        'type' => 'datetime',
        'entity_types' => array(
          'evente'
        ),
        'settings' => array(
          'todate' => 'required'
        )
      );

      $instance = array(
        'field_name' => $field_name,
        'entity_type' => 'evente',
        'bundle' => $entity->type,
        'label' => $entity->label,
        'widget' => array(
          'type' => 'date_popup',
          'settings' => array(
            'input_format' => 'Y-m-d H:i',
            'year_range' => '0:0'
          )
        )
      );
      field_create_field($field);
      field_create_instance($instance);
    }

    return $save_status;
  }

  public function getDateFieldName($entity) {
    return $entity->data['date_field_name'];
  }
}
