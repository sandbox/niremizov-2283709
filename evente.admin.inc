<?php

/**
 * @file
 * Evente editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class EventeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%'.$this->entityType;

    $items[$this->path] = array(
      'title' => 'Events',
      'description' => 'Add edit and update eventes.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of events.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities
    $items['evente/add'] = array(
      'title' => t('Add a event'),
      'description' => t('Add a new event'),
      'page callback' => 'evente_add_page',
      'access callback' => 'evente_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'evente.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Add menu items to add each different type of entity.
    foreach (evente_get_types() as $type) {
      $new_evente = evente_create(array('type' => $type->type));
      $items['evente/add/'.$type->type] = array(
        'title' => 'Add '.$type->label,
        'page callback' => 'evente_form_wrapper',
        'page arguments' => array($new_evente),
        'access callback' => 'evente_access',
        'access arguments' => array('edit', $new_evente),
        'file' => 'evente.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Menu item for viewing eventes
    $items['evente/'.$wildcard] = array(
      'title callback' => 'evente_page_title',
      'title arguments' => array(1),
      'page callback' => 'evente_page_view',
      'page arguments' => array(1),
      'access callback' => 'evente_access',
      'access arguments' => array('view', 1),
    );
    $items['evente/'.$wildcard.'/view'] = array(
      'title' => t('View'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => - 10,
      'context' => MENU_CONTEXT_PAGE,
    );
    // Loading and editing evente entities
    $items['evente/'.$wildcard.'/edit'] = array(
      'title' => t('Edit'),
      'page callback' => 'evente_form_wrapper',
      'page arguments' => array(1),
      'access callback' => 'evente_access',
      'access arguments' => array('edit', 1),
      'weight' => 0,
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'evente.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items['evente/'.$wildcard.'/delete'] = array(
      'title' => t('Delete'),
      'page callback' => 'evente_delete_form_wrapper',
      'page arguments' => array(1),
      'access callback' => 'evente_access',
      'access arguments' => array('edit', 1),
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'evente.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items['evente/change_status/'.$wildcard.'/%'] = array(
      'title' => 'Changed status for event',
      'page callback' => 'evente_change_status',
      'page arguments' => array(2,3),
      'access callback' => 'evente_access',
      'access arguments' => array('edit', 1),
      'type' => MENU_CALLBACK,
    );
    $items['evente/'.$wildcard.'/occurrence/%/%'] = array(
      'page callback' => 'evente_occurrence_form_wrapper',
      'page arguments' => array(1, 4, 3),
      'access callback' => 'evente_access',
      'access arguments' => array('edit', 1),
      'file' => 'evente.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    //We call for evente_form_wrapper to alter it later, if it was called directly
    //by this url. @see evente_form_evente_edit_form_alter().
    $items['evente/replace/'.$wildcard.'/%/%/%'] = array(
      'title' => t('Edit occurence'),
      'description' => t('Edit occurence'),
      'page callback' => 'evente_replace_form_wrapper',
      'page arguments' => array(2,3,4,5),
      'access callback' => 'evente_access',
      'access arguments' => array('edit', 2),
      'file' => 'evente.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    return $items;
  }

  /**
   * Create the markup for the add Evente Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('evente_add_list', array('content' => $content));
  }

}

/**
 * Form callback wrapper: create or edit a evente.
 *
 * @param $evente
 *   The evente object being edited by this form.
 *
 * @see evente_edit_form()
 */
function evente_form_wrapper($evente) {
  // Add the breadcrumb for the form's location.
  evente_set_breadcrumb();

  // Determine evente type.
  $type = evente_get_types($evente->type);

  // When creating new event, prepare additional arguments.
  // We can send where, who, when fields, to the form.
  // We schould make it here, not in evente_edit_form, cause
  // it is simpler to parse arguments and safer.
  $form_args = array();
  if (isset($evente->is_new) && $evente->is_new && sizeof(func_get_args()) > 1) {
    $args = func_get_args();
    $form_args = array_slice($args, '1');
  }

  return drupal_get_form('evente_edit_form', $evente, $type, $form_args);
}

/**
 * Form callback wrapper: delete an occurrence of event.
 *
 * @param $evente - Evente Object
 * @param $date - string representing date of occurrence to change
 *
 * @see evente_occurrence_change_form
 */
function evente_occurrence_form_wrapper($evente, $op, $date) {
  if ($op != 'edit' && $op != 'delete') {
    return drupal_not_found();
  }
  if (sizeof($evente->{$evente->getDateFieldName()}[LANGUAGE_NONE]) > 1) {
    return drupal_get_form('evente_occurrence_form', $evente, $op, $date);
  } else {
    module_load_include('inc', 'evente', 'evente.admin');
    $type = evente_get_types($evente->type);
    return drupal_get_form('evente_'.$op.'_form', $evente, $type);
  }
}

/**
 * Evente_edit_form wrapper for replacing the events.
 */
function evente_replace_form_wrapper($evente, $host, $place, $date) {
  if (isset($evente->project_id)) {
    $project_id = $evente->project_id;
  } else {
    $project_id = NULL;
  }
  $new_evente = evente_create(array('type' => $evente->type));
  return evente_form_wrapper($new_evente, $host, $place, $date, $project_id);
}

/**
 * Additional submit handler for evente_edit_form form.
 * Deleting occurence of certain set.
 */
function evente_replace_submit($form, &$form_state) {
  //Prepare form_state of evente, which occurence schould be deleted
  $repl_form_state = array(
    '#evente' => $form_state['#evente_replace'],
    '#occurrence' => $form_state['#evente_defaults']['date'],
  );
  evente_occurrence_form_delete_submit(array(), $repl_form_state);
}

/**
 * Form callback wrapper: delete a evente.
 *
 * @param $evente
 *   The evente object being edited by this form.
 *
 * @see evente_edit_form()
 */
function evente_delete_form_wrapper($evente) {
  // Add the breadcrumb for the form's location.
  //evente_set_breadcrumb();
  return drupal_get_form('evente_delete_form', $evente);
}

/**
 * Form callback: edit a evente.
 * @param $evente
 *   The evente object to edit or for a create form an empty evente object
 *     with only a evente type defined.
 * @param $type
 *   The evente type object.
 * @param $default
 *   Form can receive additional arguemnts, in certain order:
 *     0 => node_ref, // Nid number
 *     1 => taxonomy_ref, // Tid number
 *     2 => project_ref, // Optional, works only if project_m module enabled
 *
 */
function evente_edit_form($form, &$form_state, $evente, $type, $form_args = array()) {
  //Save defaults to the form, so we could use them later or other modules.
  $form_state['#evente_defaults'] = $defaults = _evente_edit_form_defaults($form_state, $evente, $type, $form_args);

  $form['#evente_type'] = $type;
  // Add the default field elements.
  $form['name'] = array(
    //Let's hiden title field, if titile autogeneration is enabled
    '#type' => (isset($type->data['title_autogen']) && $type->data['title_autogen']) ? 'hidden' : 'textfield',
    '#title' => t('Event Name'),
    '#default_value' => $defaults['name'],
    '#maxlength' => 255,
    //We also set field, not required if titile autogeneration enabled.
    '#required' => (isset($type->data['title_autogen']) && $type->data['title_autogen']) ? FALSE : TRUE,
  );
  $form['who_id'] = array(
    '#type' => 'textfield',
    '#title' => $type->data['node_ref_title'],
    '#default_value' => $defaults['who'],
    '#maxlength' => 255,
    '#required' => TRUE,
    '#autocomplete_path' => 'evente/autocomplete/node/'.$type->node_ref,
    '#disabled' => empty($defaults['who']) ? FALSE : TRUE, // if defaults were set, disable field.
  );
  $form['place_id'] = array(
    '#type' => 'textfield',
    '#title' => $type->data['term_ref_title'],
    '#default_value' => $defaults['place'],
    '#maxlength' => 255,
    '#required' => TRUE,
    '#autocomplete_path' => 'evente/autocomplete/taxonomy_term/'.$type->taxonomy_ref,
  );
  if (module_exists('project_m') && $type->type == 'shift') {
    //Check whatever we schould add addition ERP - Project - Link field to the form
    $form['project_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Project'),
      '#default_value' => $defaults['project'],
      '#maxlength' => 255,
      '#required' => TRUE,
      '#weight' => 2,
      '#autocomplete_path' => 'evente/autocomplete/node/project',
    );
  }

  // Add the field related form elements.
  $form_state['evente'] = $evente;
  field_attach_form('evente', $evente, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save evente'),
      '#submit' => $submit + array(
        'evente_edit_form_submit'
      )
    );
  // Added validation to the edit page , in order to avoid a duplicate.
  if ($form_state['build_info']['form_id'] == 'evente_edit_form' && module_exists('evente_signup')) {
    $form['send_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send email'),
      '#description' => t('To inform users about changes to the event.'),
      '#default_value' => FALSE
    );
  }

  if (!empty($evente->name)) {
    $destination = isset($_GET['destination']) ? drupal_get_destination() : NULL;
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete evente'),
      '#suffix' => l(t('Cancel'), !empty($destination['destination']) ? $destination['destination'] : 'evente/' . $evente->evente_id),
      '#submit' => $submit + array(
        'evente_form_submit_delete'
      ),
      '#weight' => 45
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'evente_edit_form_validate';

  //Get current menu item, so we can check if it is evente/replace url called.
  $menu_item = menu_get_item();
  if ($menu_item['path'] == 'evente/replace/%/%/%/%') {
    //Let's define evente_id which occurence schould be replaced
    $evente_id = $menu_item['map']['2']->evente_id;
    $rep_evente = $form_state['#evente_replace'] = evente_load($evente_id);

    // Add submit handler to the evente_edit form, so we could not only
    // create new event but also delete desired occurence of parent set.
    $form['actions']['submit']['#submit'][] = 'evente_replace_submit';
  }



  //Below would be triggered for both 'evente/add' and 'evente/replace' actions.
  //Setting default value for shift field if it is passed
  $date_field = $evente->getDateFieldName();
  if (isset($rep_evente)) {
    //Let's take time(H:m) of parent's date shift field and date(Y-m-d) of occurence to replace,
    //and combine this two values.
    $occ_utc_string = $form_state['#evente_defaults']['date'];
    $value =& $form[$date_field][LANGUAGE_NONE][0]['#default_value']['value'];
    $value2 =& $form[$date_field][LANGUAGE_NONE][0]['#default_value']['value2'];

    $value = substr_replace($rep_evente->{$date_field}[LANGUAGE_NONE][0]['value'], $occ_utc_string, 0, 10);
    $value2 = substr_replace($rep_evente->{$date_field}[LANGUAGE_NONE][0]['value2'], $occ_utc_string, 0, 10);

  } else if (!empty($form_state['#evente_defaults'])) {
    $occ_utc_string = $form_state['#evente_defaults']['date'];

    $value =& $form[$date_field][LANGUAGE_NONE][0]['#default_value']['value'];
    $value2 =& $form[$date_field][LANGUAGE_NONE][0]['#default_value']['value2'];
    //@TODO We schould programmatically understand what default H:m:s schould be used here,
    //and also set time with appropriate timezone.
    $value = $occ_utc_string.' 06:00:00';
    $value2 = $occ_utc_string.' 15:00:00';

  }
  return $form;
}

/**
 * Form callback: generates a form for choosening to what make an action, on while set
 * or only on one occurrence.
 *
 * @param $date - string represeting string of occurrence
 */
function evente_occurrence_form($form, &$form_state, $evente, $op, $date) {
  drupal_set_title(t('Occurrence '.$op));

  $form_state['#op'] = $op;
  $form_state['#evente'] = $evente;
  $form_state['#occurrence'] = $date;

  $description = t('This event is occurrence of event set. Do you want to @op this occurrence or whole set?', array('@op' => t($op)));
  $description .= t('</br><b>Warning</b>: If you choose "@op occurrence", it will be permanently marked as set exception.', array('@op' => t($op)));
  //Get destination, so we will back to right page after submition or cancel
  $destination = isset($_GET['destination']) ? drupal_get_destination() : NULL;

  //Constructing form
  $form['description'] = array('#markup' => $description);
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  $form['actions'][$op.'_occurrence'] = array(
    '#type' => 'submit',
    '#value' => t('@op occurrence', array('@op' => t($op))),
    '#submit' => array('evente_occurrence_form_'.$op.'_submit'),
    '#weight' => 40,
  );

  $form['actions'][$op] = array(
    '#markup' => l(t('@op set', array('@op' => t($op))), 'evente/'.$evente->evente_id.'/'.$op, array('attributes' => array('class' => array('button')), 'query' => $destination)),
    '#suffix' => l(t('Cancel'), !empty($destination['destination']) ? $destination['destination'] : 'evente/'.$evente->evente_id),
    '#weight' => 45,
  );

  return $form;
}

/**
 * Submit handler for occurence form edit action.
 * Generate url for redirecting to the event replacement form.
 */
function evente_occurrence_form_edit_submit($form, &$form_state) {
  $evente = $form_state['#evente'];

  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }

  $form_state['redirect'] = array(
    'evente/replace/'.$evente->evente_id.'/'.$evente->who_id.'/'.$evente->place_id.'/'.$form_state['#occurrence'],
    array('query' => $destination));
}

/**
 * Submit handler for occurence form, if delete action was choosed.
 */
function evente_occurrence_form_delete_submit($form, &$form_state) {
  $evente = $form_state['#evente'];
  $current_tz = new DateTimeZone('Europe/Moscow');
  $occurence = new DateObject(date('Y-m-d H:i:s', strtotime($form_state['#occurrence'])), $current_tz);

  // Take rrule of the first item as base for updating while field.
  // Rrule is the same for the all set...
  $item = $evente->{$evente->getDateFieldName()}[LANGUAGE_NONE][0];

  module_load_include('inc', 'date_api', 'date_api_ical');
  $parts = date_repeat_split_rrule($item['rrule']);
  $additions = $parts[2];
  $exceptions = $parts[1];
  $rrule = $parts[0];

  $utc_tz = new DateTimeZone('UTC');
  $occurence->setTimeZone($utc_tz);
  $occ_utc_string = $occurence->format('Y-m-d H:i:s');

  // If this date has been already excepted return.
  foreach ($exceptions as $exeption) {
    if (strstr($exeption['datetime'], $occ_utc_string))
      return;
  }

  $occurence->setTimeZone($current_tz);
  $exceptions[] = array(
    'datetime' => $occurence->format('Y-m-d'),
    'tz' => 'Europe/Moscow',
    'all_day' => 1,
    'granularity' => 'a:3:{i:0;s:4:"year";i:1;s:5:"month";i:2;s:3:"day";}',
  );

  $rrule_values = $rrule;
  $rrule_values['EXDATE'] = $exceptions;
  $rrule_values['RDATE'] = $additions;
  if (isset($rrule_values['COUNT'])) {
    // If repeating limit by count, and we are adding exeption
    // reduce count by one, so no redundant events would appear.
    $rrule_values['COUNT']--;
  }
  $field = field_info_field($evente->getDateFieldName());
  $event_dates = date_repeat_build_dates(NULL, $rrule_values, $field, $item);
  $evente->{$evente->getDateFieldName()}[LANGUAGE_NONE] = $event_dates;
  $evente->changed = time();
  $evente->save();
}

/**
 * Helper function for settings default values of the evente edit form.
 * add/evente/shift/{who_id}/{place_id}/{date}/{project_id}
 *
 * @TODO Move date field default set to - shift module, or all special functionality of shift module
 * schould be placed to evente module.
 */
function _evente_edit_form_defaults(&$form_state, $evente, $type, $form_args) {
  $name = isset($evente->name) ? $evente->name : NULL;
  $who = NULL;
  $place = NULL;
  $date = NULL;
  $project = NULL;

  if (isset($evente->is_new) && $evente->is_new && $form_args) {
    // We make additional is_new check, so it would be imposible to alter data for already
    // created evetes
    $who_id = isset($form_args[0]) ? $form_args[0] : NULL;
    $place_id = isset($form_args[1]) ? $form_args[1] : NULL;
    $date = isset($form_args[2]) ? $form_args[2] : NULL;
    $project_id = isset($form_args[3]) ? $form_args[3] : NULL;

    if ($who_id) {
      if ($who_node = node_load($who_id)) {
        if ($type->node_ref == 'form') {
          //Only if ancets_m module enabled, we want to out Surname+Name.
          $who = $who_node->field_surname[LANGUAGE_NONE][0]['safe_value'].' '.$who_node->field_name[LANGUAGE_NONE][0]['safe_value'].' [nid:'.$who_node->nid.']';
        } else {
          $who = $who_node->title.' [nid:'.$who_node->nid.']';
        }
      }
    }
    if ($project_id) {
      if ($project_node = node_load($project_id)) {
        $project = $project_node->title.' [nid:'.$project_node->nid.']';
      }
    }
    if ($place_id) {
      if ($place_term = taxonomy_term_load($place_id)) {
        $place = $place_term->name;
      }
    }
  } else if (!isset($evente->is_new)) {
    //Try to get already saved data.
    $wrapper = entity_metadata_wrapper('evente', $evente);

    if ($type->node_ref == 'form') {
      //Only if ancets_m module enabled, we want to out Surname+Name.
      $who_obj = $wrapper->who_id->value();
      //Surname Name [nid: $nid]
      $who = $who_obj->field_surname[LANGUAGE_NONE][0]['safe_value'].' '.$who_obj->field_name[LANGUAGE_NONE][0]['safe_value'].' [nid:'.$wrapper->who_id->nid->value().']';
    } else {
      $who = $wrapper->who_id->title->value().' [nid:'.$wrapper->who_id->nid->value().']';
    }
    $place = $wrapper->place_id->name->value();
    if (module_exists('project_m')) {
      $project = $wrapper->project_id->title->value().' [nid:'.$wrapper->project_id->nid->value().']';
    }
  }

  $return_array = array(
    'name' => $name,
    'who' => $who,
    'place' => $place,
    'date' => $date
  );

  if (isset($project)) {
    $return_array['project'] = $project;
  }

  return $return_array;
}

/**
 * Form API validate callback for the evente form
 */
function evente_edit_form_validate(&$form, &$form_state) {
  $evente = $form_state['evente'];
  $type = $form['#evente_type'];
  $form_state['values']['place_id'] = evente_validate_tax_field($form_state['values']['place_id'], $type->taxonomy_ref);
  $form_state['values']['who_id'] = evente_validate_noderef_field('who_id', $form_state['values']['who_id'], $type->node_ref);

  // Check whatever event created on passed date, and if it is check permissions.
  evente_validate_datefield($form_state['values'], $form['#evente_type']);

  if (isset($form_state['values']['project_id'])) {
    //Project_id could be not set, if project_m module isn't installed
    $form_state['values']['project_id'] = evente_validate_noderef_field('project_id', $form_state['values']['project_id'], 'project');
  }
  // Notify field widgets to validate their data.
  field_attach_form_validate('evente', $evente, $form, $form_state);
}

/** Copied from taxonomy_autocomplete.
 *  @see taxonomy_autocomplete()
 */
function evente_validate_tax_field($input = '', $voc) {
  // Autocomplete widgets do not send their tids in the form, so we must detect
  // them here and process them independently.
  $value = NULL;
  if ($tag = $input) {
    $vocabularies = taxonomy_vocabulary_get_names();
    $vid[] = $vocabularies[$voc]->vid;

    // Translate term names into actual terms.

    // See if the term exists in the chosen vocabulary and return the tid;
    // otherwise, create a new 'autocreate' term for insert/update.
    if ($possibilities = taxonomy_term_load_multiple(array(), array('name' => trim($tag), 'vid' => $vid))) {
      $term = array_pop($possibilities);
    } else {
      $term = NULL;
      form_set_error('place_id', t('You cannot create new terms.'));
    }
    $value = $term ? $term->tid : NULL;

  }
  return $value;
}

/**
 * @see node_reference_autocomplete_validate().
 */
function evente_validate_noderef_field($field_name ,$input = '', $bundle) {
  $value = NULL;
  $nid = NULL;

  if ($input) {
    // Check whether we have an explicit "[nid:n]" input.
    preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $input, $matches);
    if (!empty($matches)) {
      // Explicit nid.
      list(, $title, $nid) = $matches;
      if ($possibilitie = node_load($nid)) {
        $value = $possibilitie->type == $bundle ? $nid : NULL;
      }
    }
  }
  if (empty($value)) {
    form_set_error($field_name,t('Field '.$field_name.' could not have such value.'));
  }
  return $value;
}

/**
 * Check whatever date is passed and if it is check if current user have permission to create
 * event with passed date, if does not output error.
 *
 * @param $values
 *  Array of values from $form_state['values].
 * @param $evente_type
 *  Evente Type oject.
 *
 */
function evente_validate_datefield($values, $evente_type) {
  $date_field = $evente_type->data['date_field_name'];

  foreach ($values[$date_field][LANGUAGE_NONE] as $date_item) {
    if (is_string($date_item)) {
      continue;
    }
    if (!empty($date_item['timezone'])) {
      $date = new DateObject($date_item['value'], 'UTC');
      $date->setTimezone(new DateTimeZone($date_item['timezone']));
    } else {
      $date = new DateObject($date_item['value']);
    }
    $now = new DateObject(time());

    $difference = $now->difference($date, 'seconds', FALSE);

    if ($difference < 0 && !user_access('create passed date '. $evente_type->type .' eventes')) {
      // If date is a passed date, and user don't have right to create events with passed day,
      // set error.
      form_set_error($date_field, t('You have no permission to create event with passed date.'));
    }
  }
}

/**
 * Form API submit callback for the evente form.
 *
 * @todo remove hard-coded link
 */
function evente_edit_form_submit(&$form, &$form_state) {
  $type = $form['#evente_type'];
  // Save the evente and go back to the list of eventes.
  $evente = entity_ui_controller('evente')->entityFormSubmitBuildEntity($form, $form_state);
  $evente->save();
  // Added processing for letters when you change training.
  if ($form['send_email']['#value'] == 1) {
    $evente->name = t('Was changed to" ' . $evente->name);
    $user_event = $form_state['values']['evente_signup']['attendies'];
    foreach ($user_event as $by_name) {
      if (!empty($by_name)) {
        $account = user_load_by_name($by_name);
        if (!empty($form['send_email'])) {
          $operations[] = array(
            'evente_signup_mailsend',
            array(
              $account,
              $evente
            )
          );
        }
      }
    }
    $batch = array(
      'title' => t('Sending emails'),
      'operations' => $operations,
      'error_message' => t('Error')
    );
    batch_set($batch);
  }

  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
    $form_state['redirect'] = $destination;
  } else {
    $form_state['redirect'] = 'evente/' . $evente->evente_id;
  }
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function evente_form_submit_delete(&$form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }

  $form_state['redirect'] = array('evente/'.$form_state['evente']->evente_id.'/delete', array('query' => $destination));
}

/**
 * Form callback: confirmation form for deleting a evente.
 *
 * @param $evente
 *   The evente to delete
 *
 * @see confirm_form()
 */
function evente_delete_form($form, &$form_state, $evente) {
  $form_state['evente'] = $evente;

  $form['#submit'][] = 'evente_delete_form_submit';

  $form = confirm_form($form, t('Are you sure you want to delete event %name?', array('%name' => $evente->name)), 'evente/'.$evente->evente_id, '<p>'.t('This action cannot be undone.').'</p>', t('Delete'), t('Cancel'), 'confirm');

  return $form;
}

/**
 * Submit callback for evente_delete_form
 */
function evente_delete_form_submit($form, &$form_state) {
  $evente = $form_state['evente'];

  evente_delete($evente);

  drupal_set_message(t('The event %name has been deleted.', array('%name' => $evente->name)));
  watchdog('evente', 'Deleted evente %name.', array('%name' => $evente->name));

  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
    $form_state['redirect'] = $destination;
  } else {
    $form_state['redirect'] = '/';
  }
}

/**
 * Page to add Evente Entities.
 *
 * @todo Pass this through a proper theme function
 */
function evente_add_page() {
  $controller = entity_ui_controller('evente');
  return $controller->addPage();
}

/**
 * Displays the list of available evente types for evente creation.
 *
 * @ingroup themeable
 */
function theme_evente_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="evente-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>'.l($item['title'], $item['href']).'</dt>';
      $output .= '<dd>'.filter_xss_admin($item['description']).'</dd>';
    }
    $output .= '</dl>';
  } else {
    if (user_access('administer evente types')) {
      $output = '<p>'.t('Event Entities cannot be added because you have not created any evente types yet. Go to the <a href="@create-evente-type">evente type creation page</a> to add a new evente type.', array('@create-evente-type' => url('admin/structure/evente_types/add'))).'</p>';
    } else {
      $output = '<p>'.t('No evente types have been created yet for you to use.').'</p>';
    }
  }

  return $output;
}

/**
 * Sets the breadcrumb for administrative evente pages.
 */
function evente_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Events'), 'admin/content/evente'),
  );

  drupal_set_breadcrumb($breadcrumb);
}
