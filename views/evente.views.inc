<?php

/**
 * @file
 * Providing extra functionality for the Evente UI via views.
 */

/**
 * Implements hook_views_data()
 */
function evente_views_data_alter(&$data) {
  $data['evente']['link_evente'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the event.'),
      'handler' => 'evente_handler_link_field',
    ),
  );
  $data['evente']['edit_evente'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the event.'),
      'handler' => 'evente_handler_edit_link_field',
    ),
  );
  $data['evente']['delete_evente'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the event.'),
      'handler' => 'evente_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows eventes/evente/%evente_id/op
  $data['evente']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this event.'),
      'handler' => 'evente_handler_evente_operations_field',
    ),
  );

  // Add implicit relation of node table when evente is base table.
  $data['node']['table']['join']['evente'] = array(
    'left_field' => 'who_id',
    'field' => 'nid',
  );
}

/**
 * Implements hook_views_default_views().
 */
function evente_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'eventes';
  $view->description = 'A list of all eventes';
  $view->tag = 'eventes';
  $view->base_table = 'evente';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'more';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any evente type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Apply';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reset';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sort';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Sort Asc';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Sort Desc';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Items per page';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- All -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Offset';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« first';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ previous';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'next ›';
  $handler->display->display_options['pager']['options']['tags']['previous'] = 'last »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'evente_id' => 'evente_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'evente_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  /* Empty result behaviour: Global: Text field */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['content'] = 'No events have been created yet';
  /* Relationship: Event: Where */
  $handler->display->display_options['relationships']['place_id']['id'] = 'place_id';
  $handler->display->display_options['relationships']['place_id']['table'] = 'evente';
  $handler->display->display_options['relationships']['place_id']['field'] = 'place_id';
  /* Relationship: Event: Who */
  $handler->display->display_options['relationships']['who_id']['id'] = 'who_id';
  $handler->display->display_options['relationships']['who_id']['table'] = 'evente';
  $handler->display->display_options['relationships']['who_id']['field'] = 'who_id';
  /* Field: Event: Event ID */
  $handler->display->display_options['fields']['evente_id']['id'] = 'evente_id';
  $handler->display->display_options['fields']['evente_id']['table'] = 'evente';
  $handler->display->display_options['fields']['evente_id']['field'] = 'evente_id';
  /* Field: Event: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'evente';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Label';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'who_id';
  $handler->display->display_options['fields']['title']['label'] = 'Who';
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Taxonomy Term: Label */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'place_id';
  $handler->display->display_options['fields']['name_1']['label'] = 'Where';
  /* Field: Event: Link */
  $handler->display->display_options['fields']['link_evente']['id'] = 'link_evente';
  $handler->display->display_options['fields']['link_evente']['table'] = 'evente';
  $handler->display->display_options['fields']['link_evente']['field'] = 'link_evente';
  $handler->display->display_options['fields']['link_evente']['label'] = 'View';
  /* Field: Event: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'evente';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';

  /* Display: Page */
  $handler = $view->new_display('page', 'AdminPage', 'admin_page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/content/evente/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Events';
  $handler->display->display_options['tab_options']['description'] = 'Manage events';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';

  foreach (evente_get_types() as $etype_name => $evente_type) {
    $date_field = $evente_type->data['date_field_name'];
    /* Display: Evente Bundle View */
    $handler = $view->new_display('page', $evente_type->label, 'evente_' . $etype_name);
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['title'] = $evente_type->label;
    $handler->display->display_options['defaults']['use_ajax'] = FALSE;
    $handler->display->display_options['use_ajax'] = TRUE;
    $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
    $handler->display->display_options['defaults']['access'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'date_views_pager';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'calendar_style';
    $handler->display->display_options['style_options']['name_size'] = '3';
    $handler->display->display_options['style_options']['mini'] = '0';
    $handler->display->display_options['style_options']['with_weekno'] = '0';
    $handler->display->display_options['style_options']['multiday_theme'] = '1';
    $handler->display->display_options['style_options']['theme_style'] = '1';
    $handler->display->display_options['style_options']['max_items'] = '0';
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'calendar_entity';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Поле: Термин таксономии: Название */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    $handler->display->display_options['fields']['name']['relationship'] = 'place_id';
    $handler->display->display_options['fields']['name']['label'] = '';
    $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
    /* Поле: Событие: Date */
    $handler->display->display_options['fields'][$date_field]['id'] = $date_field;
    $handler->display->display_options['fields'][$date_field]['table'] = 'field_data_' . $date_field;
    $handler->display->display_options['fields'][$date_field]['field'] = $date_field;
    $handler->display->display_options['fields'][$date_field]['label'] = '';
    $handler->display->display_options['fields'][$date_field]['exclude'] = TRUE;
    $handler->display->display_options['fields'][$date_field]['element_label_colon'] = FALSE;
    $handler->display->display_options['fields'][$date_field]['settings'] = array(
      'format_type' => 'evente_year_month_day',
      'fromto' => 'value',
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'show_repeat_rule' => 'hide'
    );
    $handler->display->display_options['fields'][$date_field]['group_rows'] = FALSE;
    $handler->display->display_options['fields'][$date_field]['delta_offset'] = '0';
    /* Поле: Событие: Date */
    $handler->display->display_options['fields'][$date_field . '_1']['id'] = $date_field . '_1';
    $handler->display->display_options['fields'][$date_field . '_1']['table'] = 'field_data_' . $date_field;
    $handler->display->display_options['fields'][$date_field . '_1']['field'] = $date_field;
    $handler->display->display_options['fields'][$date_field . '_1']['label'] = '';
    $handler->display->display_options['fields'][$date_field . '_1']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields'][$date_field . '_1']['settings'] = array(
      'format_type' => 'evente_hour_min',
      'fromto' => 'both',
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'show_repeat_rule' => 'hide'
    );
    $handler->display->display_options['fields'][$date_field . '_1']['group_rows'] = FALSE;
    $handler->display->display_options['fields'][$date_field . '_1']['delta_offset'] = '0';
    /* Поле: Событие: Кто */
    $handler->display->display_options['fields']['who_id']['id'] = 'who_id';
    $handler->display->display_options['fields']['who_id']['table'] = 'evente';
    $handler->display->display_options['fields']['who_id']['field'] = 'who_id';
    $handler->display->display_options['fields']['who_id']['label'] = '';
    $handler->display->display_options['fields']['who_id']['exclude'] = TRUE;
    $handler->display->display_options['fields']['who_id']['alter']['strip_tags'] = TRUE;
    $handler->display->display_options['fields']['who_id']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['who_id']['separator'] = '';
    /* Поле: Событие: Событие ID */
    $handler->display->display_options['fields']['evente_id']['id'] = 'evente_id';
    $handler->display->display_options['fields']['evente_id']['table'] = 'evente';
    $handler->display->display_options['fields']['evente_id']['field'] = 'evente_id';
    $handler->display->display_options['fields']['evente_id']['label'] = '';
    $handler->display->display_options['fields']['evente_id']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['evente_id']['alter']['text'] = 'View';
    $handler->display->display_options['fields']['evente_id']['alter']['make_link'] = TRUE;
    $handler->display->display_options['fields']['evente_id']['alter']['path'] = 'evente/[evente_id]';
    $handler->display->display_options['fields']['evente_id']['alter']['strip_tags'] = TRUE;
    $handler->display->display_options['fields']['evente_id']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['evente_id']['separator'] = '';
      /* Поле: Событие: Событие ID */
    $handler->display->display_options['fields']['evente_id_1']['id'] = 'evente_id';
    $handler->display->display_options['fields']['evente_id_1']['table'] = 'evente';
    $handler->display->display_options['fields']['evente_id_1']['field'] = 'evente_id';
    $handler->display->display_options['fields']['evente_id_1']['label'] = '';
    $handler->display->display_options['fields']['evente_id_1']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['evente_id_1']['alter']['text'] = 'Edit';
    $handler->display->display_options['fields']['evente_id_1']['alter']['make_link'] = TRUE;
    $handler->display->display_options['fields']['evente_id_1']['alter']['path'] = 'evente/[evente_id_1]/occurrence/[' . $etype_name . ']/edit?destination=node/[who_id]/' . $etype_name;
    $handler->display->display_options['fields']['evente_id_1']['alter']['strip_tags'] = TRUE;
    $handler->display->display_options['fields']['evente_id_1']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['evente_id_1']['separator'] = '';
    /* Поле: Событие: Событие ID */
    $handler->display->display_options['fields']['evente_id_2']['id'] = 'evente_id_1';
    $handler->display->display_options['fields']['evente_id_2']['table'] = 'evente';
    $handler->display->display_options['fields']['evente_id_2']['field'] = 'evente_id';
    $handler->display->display_options['fields']['evente_id_2']['label'] = '';
    $handler->display->display_options['fields']['evente_id_2']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['evente_id_2']['alter']['text'] = 'Delete';
    $handler->display->display_options['fields']['evente_id_2']['alter']['make_link'] = TRUE;
    $handler->display->display_options['fields']['evente_id_2']['alter']['path'] = 'evente/[evente_id_2]/occurrence/[' . $date_field . ']/delete?destination=node/[who_id]/' . $etype_name;
    $handler->display->display_options['fields']['evente_id_2']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['evente_id_2']['separator'] = '';
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Контекстный фильтр: Содержимое: Nid */
    $handler->display->display_options['arguments']['nid']['id'] = 'nid';
    $handler->display->display_options['arguments']['nid']['table'] = 'node';
    $handler->display->display_options['arguments']['nid']['field'] = 'nid';
    $handler->display->display_options['arguments']['nid']['relationship'] = 'who_id';
    $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
    $handler->display->display_options['arguments']['nid']['exception']['value'] = '';
    $handler->display->display_options['arguments']['nid']['exception']['title'] = 'Все';
    $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
    $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['nid']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['nid']['validate']['type'] = 'node';
    $handler->display->display_options['arguments']['nid']['validate_options']['types'] = array(
      $evente_type->node_ref => $evente_type->node_ref,
    );
    $handler->display->display_options['arguments']['nid']['validate_options']['access'] = TRUE;
    /* Контекстный фильтр: Дата: Дата (evente) */
    $handler->display->display_options['arguments']['date_argument']['id'] = 'date_argument';
    $handler->display->display_options['arguments']['date_argument']['table'] = 'evente';
    $handler->display->display_options['arguments']['date_argument']['field'] = 'date_argument';
    $handler->display->display_options['arguments']['date_argument']['default_action'] = 'default';
    $handler->display->display_options['arguments']['date_argument']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['date_argument']['date_fields'] = array(
      'field_data_' . $date_field . '.' . $date_field . '_value' => 'field_data_' . $date_field . '.' . $date_field . '_value',
      'field_data_' . $date_field . '.' . $date_field . '_value2' => 'field_data_' . $date_field . '.' . $date_field . '_value2'
    );
    $handler->display->display_options['path'] = 'node/%/' . $etype_name;
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = $evente_type->label;
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['tab_options']['type'] = 'tab';
    $handler->display->display_options['tab_options']['title'] = $evente_type->label;
    $handler->display->display_options['tab_options']['weight'] = '0';
  }

  $translatables['eventes'] = array(
    t('Master'),
    t('Events'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort'),
    t('Sort Asc'),
    t('Sort Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Empty '),
    t('No events have been created yet'),
    t('Event ID'),
    t('.'),
    t(','),
    t('Label'),
    t('Who'),
    t('Where'),
    t('View'),
    t('Operations links'),
    t('Page'),
    t('All'),
  );

  $views[$view->name] = $view;

  return $views;

}
