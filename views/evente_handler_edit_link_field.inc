<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class evente_handler_edit_link_field extends evente_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};
    
    //Creating a dummy evente to check access against
    $dummy_evente = (object) array('type' => $type);
    if (!evente_access('edit', $dummy_evente)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $evente_id = $values->{$this->aliases['evente_id']};
    
    return l($text, '/evente/' . $evente_id . '/edit');
  }
}
