<?php

/**
 * @TODO Use cache for SQL queries.
 * 
 * @param $entityType - Taxonomy_term or Node
 * @param $propertie - Vocabulary or Node type
 * @see taxonomy_autocomplete()
 * @see node_reference_autocomplete()
 */
function evente_autocomplete($entityType, $propetrie) {
  $matches = array();
  $args = func_get_args();
  $tag_typed = drupal_strtolower(array_pop($args));
  if ($tag_typed != '') {
    switch ($entityType) {
    case 'taxonomy_term':
      // Get VID
      $vocabularies = taxonomy_vocabulary_get_names();
      $vid = $vocabularies[$propetrie]->vid;

      $query = db_query("SELECT tid,name FROM taxonomy_term_data WHERE  (vid IN  (:vid)) AND (name LIKE :tags_typed ESCAPE '\\\\') LIMIT 10 OFFSET 0;", array(':vid' => $vid, ':tags_typed' => '%'.$tag_typed.'%'));
      $result = $query->fetchAllKeyed();
      //Create result array tid=>name
      foreach ($result as $tid => $name) {
        $n = $name;
        $matches[$n] = check_plain($name);
      }

      break;
    case 'node':
      $matches = array();
      $query = db_select('node', 'n');
      $query->addField('n', 'nid');
      $query->addField('n', 'type',  'node_type');
      $query->addTag('node_access');
      
      if ($propetrie == 'form') {
        //Bundle == form
        $args = array(':node' => 'node');
        //We want title of form bundle, be an combination of Surname And Name, so make appropriate joins 
        $query->join('field_data_field_name', 'name', 'n.nid = name.entity_id AND name.entity_type = :node', $args);
        $query->join('field_data_field_surname', 'surname', 'n.nid = surname.entity_id AND surname.entity_type = :node', $args);
        //Past together name and surname
        $query->addExpression('CONCAT(surname.field_surname_value, :space, name.field_name_value)', 'node_title', array(':space' => ' '));
        //Check whatever entered text match node_title field.
        $query->where('CONCAT(surname.field_surname_value, :space, name.field_name_value) LIKE :tagtype', array(':space' => ' ', ':tagtype' => '%'.$tag_typed.'%'));
      } else {
          //Bundle != Form
          $query->addField('n', 'title', 'node_title');
          $query->condition('n.title', '%' . $tag_typed . '%', 'LIKE');
      }
      
      $query->condition('n.status', '1', '=');
      $query->condition('n.type', array($propetrie), 'IN');
      $query->range(0, 10);
      
      $result = $query->execute()->fetchAll();
      
      $references = array();
      foreach ($result as $node) {
        $references[$node->nid] = array(
          'title'    => $node->node_title,
          'rendered' => check_plain($node->node_title),
        );
      }
      
      foreach ($references as $id => $row) {
        // Markup is fine in autocompletion results (might happen when rendered
        // through Views) but we want to remove hyperlinks.
        $suggestion = preg_replace('/<a href="([^<]*)">([^<]*)<\/a>/', '$2', $row['rendered']);
        // Add a class wrapper for a few required CSS overrides.
        $matches[$row['title'] . " [nid:$id]"] = '<div class="reference-autocomplete">' . $suggestion . '</div>';
      }
      
      break;
    }
  }
  drupal_json_output($matches);
}

